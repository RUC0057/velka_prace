﻿from django.db import models
from django.core.validators import MinLengthValidator, MaxLengthValidator

class Student(models.Model):
    login = models.CharField(max_length = 6, validators=[MaxLengthValidator(6),MinLengthValidator(6)])
    first_name = models.CharField(max_length=15)
    last_name = models.CharField(max_length=15)
    email = models.CharField(max_length=25)
    grade = models.IntegerField()
    date_of_birth = models.DateTimeField()

    def __str__(self):
        return "{} {} {}".format(self.login, self.first_name, self.last_name)

class Scholarship(models.Model):
    name = models.CharField(max_length=30)
    amount = models.IntegerField()
    student = models.ForeignKey('Student', on_delete=models.CASCADE)

    def __str__(self):
        return "{} {} {}".format(self.name, self.amount, self.student.login)

class Teacher(models.Model):
    login = models.CharField(max_length = 6,validators=[MaxLengthValidator(6),MinLengthValidator(6)])
    first_name = models.CharField(max_length=15)
    last_name = models.CharField(max_length=15)
    email = models.CharField(max_length=25)
    department = models.CharField(max_length=30)

    def __str__(self):
        return "{} {} {}".format(self.login, self.first_name, self.last_name)

class Subject(models.Model):
    short_name = models.CharField(max_length=10)
    name = models.CharField(max_length=40)
    credits = models.IntegerField()
    way_of_complete = models.CharField(max_length=30)
    teacher = models.ForeignKey('Teacher', on_delete=models.CASCADE)

    def __str__(self):
        return "{} {}".format(self.short_name, self.name)

class Classroom(models.Model):
    code = models.CharField(max_length=10)
    capacity = models.IntegerField()
    building = models.CharField(max_length=30)

    def __str__(self):
        return "{}".format(self.code)

class StudentCourse(models.Model):
    year = models.IntegerField()
    semester = models.CharField(max_length=1,validators=[MaxLengthValidator(1),MinLengthValidator(1)])
    points = models.IntegerField()
    subject = models.ForeignKey('Subject', on_delete=models.CASCADE)
    classroom = models.ForeignKey('Classroom', on_delete=models.CASCADE)
    student = models.ForeignKey('Student', on_delete=models.CASCADE)

    def __str__(self):
        return "{} {} {} {}".format(self.year, self.semester, self.subject.name, self.student.login)

    def is_passed(self):
        if self.points < 51:
            return '\u274c'
        else:
            return '✔'
