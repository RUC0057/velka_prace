"""
Definition of views.
"""

from datetime import datetime
from django.http.response import Http404
from django.shortcuts import render
from django.http import HttpRequest
from django.http import HttpResponse
from app.models import Student, Teacher, Classroom, StudentCourse, Scholarship, Subject
from app.forms import StudentForm, StudentFilter, StudentCourseForm, TeacherFilter, TeacherForm, TeacherSubjectForm, ClassroomForm


def homepage(request):
    return render(request, 'homepage.html')

def students(request):
    if request.method == 'POST':
        form = StudentFilter(request.POST)
        if form.is_valid():
            l = form.cleaned_data['login']
            fn = form.cleaned_data['first_name']
            ln = form.cleaned_data['last_name']
            g = form.cleaned_data['grade']
            if (fn is '' and ln is '' and g is None):
                students = Student.objects.filter(login=l)
            elif (l is '' and ln is '' and g is None):
                students = Student.objects.filter(first_name=fn)
            elif (l is '' and fn is '' and g is None):
                students = Student.objects.filter(last_name=ln)
            elif (l is '' and fn is '' and ln is ''):
                students = Student.objects.filter(grade=g)
            elif(ln is '' and g is None):
                students = Student.objects.filter(login=l, first_name = fn)
            elif(fn is '' and g is None):
                students = Student.objects.filter(login=l, last_name = ln)
            elif(fn is '' and ln is ''):
                students = Student.objects.filter(login=l, grade = g)
            elif(l is '' and g is None):
                students = Student.objects.filter(first_name=fn, last_name = ln)
            elif(l is '' and ln is ''):
                students = Student.objects.filter(first_name=fn, grade =g)
            elif(l is '' and fn is ''):
                students = Student.objects.filter(last_name=ln, grade = g)
            elif(l is ''):
                students = Student.objects.filter(first_name=fn, last_name = ln, grade = g)
            elif(fn is ''):
                students = Student.objects.filter(login=l, last_name = ln, grade = g)
            elif(ln is ''):
                students = Student.objects.filter(login = l, first_name=fn, grade = g)
            elif(g is None):
                students = Student.objects.filter(login = l, first_name=fn, last_name = ln)
            else:
                students = Student.objects.filter(login=l, first_name= fn, last_name= ln, grade = g)

        else:
            students = Student.objects.all()
    else:
        form = StudentFilter()
        students = Student.objects.all()

    return render(request, 'students.html', {'students':students, 'form':form})

def teachers(request):
    if request.method == 'POST':
        form = TeacherFilter(request.POST)
        if form.is_valid():
            l = form.cleaned_data['login']
            fn = form.cleaned_data['first_name']
            ln = form.cleaned_data['last_name']
            if (fn is '' and ln is ''):
                teachers = Teacher.objects.filter(login=l)
            elif (l is '' and ln is ''):
                teachers = Teacher.objects.filter(first_name=fn)
            elif (l is '' and fn is ''):
                teachers = Teacher.objects.filter(last_name=ln)
            elif(l is ''):
                teachers = Teacher.objects.filter(first_name=fn, last_name = ln)
            elif(fn is ''):
                teachers = Teacher.objects.filter(login=l, last_name = ln)
            elif(ln is ''):
                teachers = Teacher.objects.filter(login = l, first_name=fn)
            else:
                teachers = Teacher.objects.filter(login=l, first_name= fn, last_name= ln)

        else:
            teachers = Teacher.objects.all()
    else:
        form = TeacherFilter
        teachers = Teacher.objects.all()

    return render(request, 'teachers.html', {'teachers':teachers, 'form':form})


def scholarships(request, student_id):
    try:
        s = Student.objects.get(id = student_id)
    except Student.DoesNotExist:
        raise Http404("Student nenalezen")
    try: 
        ss = Scholarship.objects.filter(student = s)
    except:
        raise Http404("Student nema zadna stipendia")

    return render(request, 'scholarships.html', {'scholarships':ss})


def subjects(request, student_id):
    try:
        s = Student.objects.get(id = student_id)
    except Student.DoesNotExist:
        raise Http404("Student nenalezen")
    try: 
        ss = StudentCourse.objects.filter(student = s)
    except:
        raise Http404("Student nema zadne predmety")

    return render(request, 'subjects.html', {'subjects':ss})

def teacher_subjects(request, teacher_id):
    try:
        t = Teacher.objects.get(id = teacher_id)
    except Student.DoesNotExist:
        raise Http404("Ucitel nenalezen")
    try: 
        ss = Subject.objects.filter(teacher = t)
    except:
        raise Http404("Ucitel nema zadne predmety")

    return render(request, 'teacher_subjects.html', {'subjects':ss})


def student_edit(request, student_id):
    try:
        s = Student.objects.get(id = student_id)
    except Student.DoesNotExist:
        raise Http404("Student nenalezen")
    if request.method == 'POST':
        form = StudentForm(request.POST, instance = s)
        if form.is_valid():
            form.save()
    else:
        form = StudentForm(instance = s)
    return render(request, 'student_edit.html', {'form':form})

def teacher_edit(request, teacher_id):
    try:
        t = Teacher.objects.get(id = teacher_id)
    except Teacher.DoesNotExist:
        raise Http404("Ucitel nenalezen")
    if request.method == 'POST':
        form = TeacherForm(request.POST, instance = t)
        if form.is_valid():
            form.save()
    else:
        form = TeacherForm(instance = t)
    return render(request, 'teacher_edit.html', {'form':form})

def subject_edit(request, subject_id):
    try:
        s = StudentCourse.objects.get(id = subject_id)
    except StudentCourse.DoesNotExist:
        raise Http404("Predmet nenalezen")
    if request.method == 'POST':
        form = StudentCourseForm(request.POST, instance = s)
        if form.is_valid():
            form.save()
    else:
        form = StudentCourseForm(instance = s)
    return render(request, 'subject_edit.html', {'form':form})

def teacher_subject_edit(request, teacher_subject_id):
    try:
        s = Subject.objects.get(id = teacher_subject_id)
    except StudentCourse.DoesNotExist:
        raise Http404("Predmet nenalezen")
    if request.method == 'POST':
        form = TeacherSubjectForm(request.POST, instance = s)
        if form.is_valid():
            form.save()
    else:
        form = TeacherSubjectForm(instance = s)
    return render(request, 'teacher_subject_edit.html', {'form':form})

def classrooms(request):
    classrooms = Classroom.objects.all()
    return render(request, 'classrooms.html', {'classrooms':classrooms})

def classroom_edit(request, classroom_id):
    try:
        c = Classroom.objects.get(id = classroom_id)
    except Classroom.DoesNotExist:
        raise Http404("Ucebna nenalezen")
    if request.method == 'POST':
        form = ClassroomForm(request.POST, instance = t)
        if form.is_valid():
            form.save()
    else:
        form = ClassroomForm(instance = c)
    return render(request, 'classroom_edit.html', {'form':form})


def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title':'Home Page',
            'year':datetime.now().year,
        }
    )

def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        {
            'title':'Contact',
            'message':'Your contact page.',
            'year':datetime.now().year,
        }
    )

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        {
            'title':'About',
            'message':'Your application description page.',
            'year':datetime.now().year,
        }
    )
