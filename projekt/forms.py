"""
Definition of forms.
"""

from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from app.models import Student, Teacher, Scholarship, Subject, StudentCourse, Classroom

class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['grade']

class TeacherForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = ['department']

class ClassroomForm(forms.ModelForm):
    class Meta:
        model = Classroom
        fields = ['code', 'capacity']

class StudentCourseForm(forms.ModelForm):
    class Meta:
        model = StudentCourse
        fields = ['points']

class TeacherSubjectForm(forms.ModelForm):
    class Meta:
        model = Subject
        fields = ['credits', 'way_of_complete']

class StudentFilter(forms.Form):
    login = forms.CharField(label = 'Login', required = False)
    first_name = forms.CharField(label = 'First name', required = False)
    last_name = forms.CharField(label = 'Last name', required = False)
    grade = forms.IntegerField(label = 'Grade', required = False)

class TeacherFilter(forms.Form):
    login = forms.CharField(label = 'Login', required = False)
    first_name = forms.CharField(label = 'First name', required = False)
    last_name = forms.CharField(label = 'Last name', required = False)


class BootstrapAuthenticationForm(AuthenticationForm):
    """Authentication form which uses boostrap CSS."""
    username = forms.CharField(max_length=254,
                               widget=forms.TextInput({
                                   'class': 'form-control',
                                   'placeholder': 'User name'}))
    password = forms.CharField(label=_("Password"),
                               widget=forms.PasswordInput({
                                   'class': 'form-control',
                                   'placeholder':'Password'}))
