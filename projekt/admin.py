from django.contrib import admin
from app.models import Student, Teacher, Subject, Classroom, StudentCourse, Scholarship

admin.site.register(Student)
admin.site.register(Teacher)
admin.site.register(Subject)
admin.site.register(Classroom)
admin.site.register(StudentCourse)
admin.site.register(Scholarship)