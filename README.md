# Velká práce

Jedná se o projekt v Pythonu, Djangu. Tento projekt představuje jakousi část školního systému, která zajišťuje správu studentů - přehled zápočtů, přehled zapsaných předmětů. Dále je zde správa učitelů, kde je možno vidět, jaké předměty učí. V neposlední řadě je zde správa jednotlivých učeben na škole.
